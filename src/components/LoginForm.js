import { Form, Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";


export default function LoginForm({ userInput }) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isComplete, setIsComplete] = useState(false);

    useEffect(() => {
        if (email !== '' && password !== '') {
            setIsComplete(true);
        } else {
            setIsComplete(false);
        }
    })

    const login = (event) => {
        event.preventDefault();

        userInput([{ email, password }, { setEmail, setPassword }]);
    }

    return (
        <Form onSubmit={event => login(event)}>
            <Form.Group>
                <Form.Label>
                    Email:
                </Form.Label>
                <Form.Control type='email' placeholder='Insert Email Here' value={email} onChange={e => setEmail(e.target.value)} />
            </Form.Group>
            <Form.Group>
                <Form.Label>
                    Password:
                </Form.Label>
                <Form.Control type='password' placeholder='Insert Password Here' value={password} onChange={e => setPassword(e.target.value)} />
            </Form.Group>
            {
                isComplete ? <Button className='btn-block btn-primary' type='submit'>Login</Button> : <Button className='btn-block btn-danger' type='submit' disabled>Login</Button>
            }
            <p className="text-center mt-3">Don't have an account? <Link to='/register'>Register Here</Link> </p>
        </Form>
    )
}
