
export default function Banner({ data, margtop }) {

    return (
        <div className="p-5 text-center" style={{ marginTop: margtop }}>
            <h1>{data.title}</h1>
            <p>{data.tagline}</p>
        </div>
    )
}
