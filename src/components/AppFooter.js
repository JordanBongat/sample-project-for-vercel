import { Container } from "react-bootstrap";

export default function AppFooter() {
    return (
        <div id="foot">
            <Container className="d-flex flex-md-row flex-column justify-content-between text-center" id="footerContent">
                <div>&copy;Copyright 2021, All Rights Reserved</div>
                <div>Ecommerce App</div>
                <div>Contact Us: +639186123543</div>
            </Container>
        </div>
    )
}
