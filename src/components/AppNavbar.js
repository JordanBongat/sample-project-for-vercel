import { Nav, Navbar, Form, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { useState, useEffect } from "react";



export default function AppNavbar() {
    //for style
    const [open, setOpen] = useState(false);

    useEffect(() => {
        if (!open) {
            document.getElementById("buttons").style.display = "none"
        } else {
            document.getElementById("buttons").style.display = "flex"
        }
    });

    return (
        <Navbar id="navbar">
            <div className="d-flex">
                <Navbar.Brand as={NavLink} to='/' className='text-light'><h5>CS3 Ecommerce App</h5></Navbar.Brand>
                <Form className="d-flex ml-4">
                    <Form.Control type="search" placeholder="Search" className="mr-2" aria-label="Search" id="searchBar" />
                    <Button variant="outline-light"><img src="https://img.icons8.com/material-outlined/24/000000/search--v1.png" /></Button>
                </Form>
            </div>



            <div id="navigator" onMouseEnter={() => setOpen(true)} onMouseLeave={() => setOpen(false)}>
                <img src={'./navbaricon.png'} id="menu" />
                {
                    sessionStorage.getItem('accesstoken') === 'undefined' || sessionStorage.getItem('accesstoken') === null ?
                        <div id="buttons">
                            <Nav.Link as={NavLink} to='/register' id="register"><img src={'./registericon.png'} /><p>Register</p></Nav.Link>
                            <Nav.Link as={NavLink} to='/login' id="login"><img src={'./loginicon.png'} /><p>Login</p></Nav.Link>
                        </div>
                        :
                        <div id="buttons">
                            <Nav.Link as={NavLink} to='/cart' id="cart"><img src={'./carticon.png'} /> <p>Cart</p></Nav.Link>
                            <Nav.Link as={NavLink} to='/order' id="order"><img src={'./ordericon.png'} /><p>Order History</p></Nav.Link>
                            <Nav.Link as={NavLink} to='/profile' id="profile"><img src={'./profileicon.png'} /><p>Profile</p></Nav.Link>
                            <Nav.Link as={NavLink} to='/admin' id="sell"><img src={'./sellicon.png'} /><p>Sell Here</p></Nav.Link>
                        </div>
                }
            </div>
        </Navbar>
    )
}
