import { Form, Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import Swal from 'sweetalert2'
import { Link } from "react-router-dom";

export default function RegisterForm({ register }) {

    const [firstName, setFirstName] = useState('');
    const [middleInitial, setMiddleInitial] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');

    const [isComplete, setIsComplete] = useState(false);
    const [pwMatched, setPwMatched] = useState('');

    useEffect(() => {
        if ((firstName !== '' && middleInitial !== '' && lastName !== '' && email !== '' && password !== '' && password2 !== '') && (password === password2)) {
            setIsComplete(true);
        } else {
            setIsComplete(false);
        }
    })

    useEffect(() => {
        if (password === '' || password2 === '') {
            setPwMatched('');
        } else if (password === password2) {
            setPwMatched(true);
        } else {
            setPwMatched(false);
        }
    })

    const registerUser = (event) => {
        event.preventDefault();
        event.persist();

        register({ firstName, middleInitial, lastName, email, password })

        Swal.fire({
            title: 'RegisteredSuccessfully!',
            icon: 'success',
            text: 'Your New Account Has Been Created',
        }).then(result => {
            if (result.isConfirmed || result.isDismissed) {
                window.location.href = '/login';
            }
        })
    }

    return (
        <Form onSubmit={event => registerUser(event)}>
            <Form.Group>
                <Form.Label>
                    First Name:
                </Form.Label>
                <Form.Control type='text' placeholder='Insert your First Name Here' value={firstName} onChange={e => setFirstName(e.target.value)} required />
            </Form.Group>
            <Form.Group>
                <Form.Label>
                    Middle Initial:
                </Form.Label>
                <Form.Control type='text' placeholder='Insert your Middle Initial Here' value={middleInitial} onChange={e => setMiddleInitial(e.target.value)} required />
            </Form.Group>
            <Form.Group>
                <Form.Label>
                    Last Name:
                </Form.Label>
                <Form.Control type='text' placeholder='Insert your Last Name Here' value={lastName} onChange={e => setLastName(e.target.value)} required />
            </Form.Group>
            <Form.Group>
                <Form.Label>
                    Email Address:
                </Form.Label>
                <Form.Control type='email' placeholder='Insert your Email Address Here' value={email} onChange={e => setEmail(e.target.value)} required />
            </Form.Group>
            <Form.Group>
                <Form.Label>
                    Password:
                </Form.Label>
                <Form.Control type='password' placeholder='Insert your Password Here' value={password} onChange={e => setPassword(e.target.value)} required />
            </Form.Group>
            <Form.Group>
                <div className='d-flex flex-row'>
                    <Form.Label>
                        Confirm Password:
                    </Form.Label>
                    {pwMatched === '' ? <p></p> : pwMatched === true ? <p className="text-success ml-3 mb-0">*Passwords Matched*</p> : <p className="text-danger ml-3 mb-0">*Passwords Doesn't Matched*</p>}
                </div>
                <Form.Control type='password' placeholder='Verify your Password Here' value={password2} onChange={e => setPassword2(e.target.value)} required />
            </Form.Group>
            {isComplete ? <Button className='btn-block btn-primary' type="submit">Register Now</Button> : <Button className='btn-block btn-danger' type="submit" disabled>Register Now</Button>}
            <p className="text-center mt-3">Already have an account? <Link to='/login'>Login Here</Link> </p>
        </Form>
    )
}
