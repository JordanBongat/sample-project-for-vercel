import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Products({ data, id, select }) {

    return (
        <Link to={`/product/${id}`} onClick={() => select(id)}>
            <Card id='products' className="bg-light">
                <Card.Body>
                    <Card.Title>
                        {data.name}
                    </Card.Title>
                    <Card.Subtitle>
                        &#8369;{data.price}
                    </Card.Subtitle>
                    <Card.Text>
                        {data.description}
                    </Card.Text>
                </Card.Body>
            </Card>
        </Link>
    )
}
