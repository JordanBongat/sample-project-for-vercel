import { Container } from "react-bootstrap";
import { Form } from "react-bootstrap";
import { useState } from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { useHistory } from "react-router";

export default function CreateOrder() {

    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    const history = useHistory();

    const createProduct = (e) => {
        e.preventDefault();

        fetch('https://glacial-chamber-80958.herokuapp.com/products/adminCreateProduct', {
            method: "POST",
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${sessionStorage.getItem('accesstoken')}`
            },
            body: JSON.stringify({
                name: productName,
                description: description,
                price: price
            })
        })
        Swal.fire({
            icon: 'success',
            title: 'Product Creation Successful',
        }).then(res => {
            if (res.isConfirmed || res.isDismissed) {
                history.push('/admin');
            }
        })
    }

    return (
        <Container id='main'>
            <h1 className='my-5'>Create Product</h1>
            <Form onSubmit={e => createProduct(e)}>
                <Form.Group>
                    <Form.Label>
                        Product Name:
                    </Form.Label>
                    <Form.Control type='text' value={productName} onChange={e => setProductName(e.target.value)} required />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Description:
                    </Form.Label>
                    <Form.Control type='text' value={description} onChange={e => setDescription(e.target.value)} required />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Price:
                    </Form.Label>
                    <Form.Control type='number' value={price} onChange={e => setPrice(e.target.value)} required />
                </Form.Group>
                <Button type="submit">Create</Button>
            </Form>
        </Container>
    )
}
