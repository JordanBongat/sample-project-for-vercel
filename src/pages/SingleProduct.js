import { Container } from "react-bootstrap";
import { Card } from "react-bootstrap";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function SingleProduct({ data }) {

    const addToCart = (userData) => {
        fetch('https://glacial-chamber-80958.herokuapp.com/users/createOrder', {
            method: 'POST',
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('accesstoken'),
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                productId: userData._id,
                quantity: 1
            })
        })

        Swal.fire({
            position: 'bottom-end',
            icon: 'success',
            title: 'Successfully Added to Cart',
            showConfirmButton: false,
            timer: 1600
        })
    }

    return (
        <Container id='main' style={{ marginTop: '7vh' }}>
            <Card className='d-flex flex-row justify-content-start'>
                <Card.Img src='./favicon.ico' style={{ width: '500px', height: '500px' }} />
                <Card.Body className='text-center pt-5'>
                    <Card.Title style={{ fontSize: '35px' }}>
                        {data.name}
                    </Card.Title>
                    <Card.Subtitle style={{ fontSize: '25px' }}>
                        &#8369;{data.price}
                    </Card.Subtitle>
                    <Card.Text style={{ fontSize: '20px' }}>
                        {data.description}
                    </Card.Text>
                    <div className='d-flex flex-column' style={{ marginTop: '200px' }}>
                        <Button variant='primary mb-2' style={{ borderRadius: '10px' }}>Buy Now!</Button>
                        <Button variant='secondary' style={{ borderRadius: '10px' }} onClick={() => { addToCart(data) }}>Add to Cart</Button>
                    </div>
                </Card.Body>
            </Card>
        </Container>
    )
}
