import { Container } from "react-bootstrap";
import { useEffect, useState } from 'react';
import { Button } from "react-bootstrap";
import Banner from "../components/Banner";

export default function Profile() {

    const [userData, setUserData] = useState();

    useEffect(() => {
        fetch('https://glacial-chamber-80958.herokuapp.com/users/details', {
            method: "GET",
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('accesstoken')
            }
        })
            .then(res => res.json())
            .then(data => setUserData(data[0]))
    }, [])

    const logout = () => {
        sessionStorage.clear()
        window.location.href = '/'
    }

    const profileBanner = {
        title: 'Profile Page',
        tagline: 'This is your account details'
    }

    return (
        <Container id="main" className={'text-center'}>
            <Banner data={profileBanner} />
            {
                userData ? <div>
                    <div className='d-flex flex-row justify-content-center my-4'>
                        <h4 className='mx-4'>Name:</h4>
                        <h4 className='mx-4'>{`${userData.firstName} ${userData.middleInitial} ${userData.lastName}`}</h4>
                    </div>
                    <div className='d-flex flex-row justify-content-center my-4'>
                        <h4 className='mx-4'>Email:</h4>
                        <h4 className='mx-4'>{userData.email}</h4>
                    </div>
                    <Button variant="danger my-5" onClick={() => logout()}>Logout</Button>
                </div> : <div></div>
            }
        </Container>
    )
}
