import { Container } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { Card } from "react-bootstrap";
import { useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import { Form } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Cart() {

    const [products, setProducts] = useState();

    useEffect(() => {
        fetch('https://glacial-chamber-80958.herokuapp.com/users/retrieveOrder', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('accesstoken')
            }
        })
            .then(res => res.json())
            .then(data => setProducts(data))
    }, [])

    const handleAdd = (product) => {
        setProducts(products.map((item) => item._id === product._id ? { ...item, quantity: item.quantity + 1 } : item))
    }

    const handleMinus = (product) => {
        setProducts(products.map((item) => {
            if (item.quantity > 1) {
                return item._id === product._id ? { ...item, quantity: item.quantity - 1 } : item
            } else {
                return item;
            }
        }))
    }

    const removeProduct = (product) => {
        const update = products.filter((item) => item._id !== product._id && item);

        fetch('https://glacial-chamber-80958.herokuapp.com/users/removeCartProduct', {
            method: 'POST',
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('accesstoken'),
                'Content-type': 'application/json'
            },
            body: JSON.stringify(update)
        })
            .then(res => res.json())
            .then(data => setProducts(data.orders))
    }

    const checkOutOrder = () => {
        fetch('https://glacial-chamber-80958.herokuapp.com/users/checkoutOrder', {
            method: 'POST',
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('accesstoken'),
                'Content-type': 'application/json'
            },
            body: JSON.stringify(products)
        })

        fetch('https://glacial-chamber-80958.herokuapp.com/users/removeCartProduct', {
            method: 'POST',
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('accesstoken'),
                'Content-type': 'application/json'
            },
            body: JSON.stringify([])
        })
            .then(res => res.json())
            .then(data => setProducts(data.orders))

        Swal.fire({
            icon: 'success',
            title: 'Alright!',
            text: 'Your order is successful'
        }).then(() => {
            window.location.href = '/cart'
        })

    }

    return (
        <Container id="main">
            <h1 className='text-center mt-5 mb-5'>
                Shopping Cart
            </h1>
            <Card>
                <Card.Body id="cart-head" style={{ backgroundColor: 'Menu' }}>
                    <Row id='cart-row-head'>
                        <Col xs={5} className='pl-5' style={{ textAlign: 'start' }}>
                            <h4>Product Name</h4>
                        </Col>
                        <Col>
                            <h4>Quantity</h4>
                        </Col>
                        <Col>
                            <h4>Price</h4>
                        </Col>
                        <Col>
                            <h4>Subtotal</h4>
                        </Col>
                        <Col>

                        </Col>
                    </Row>
                </Card.Body>
            </Card>
            {
                !products || products.length === 0 ?
                    <Card>
                        <Card.Body>
                            <h5 className="text-center" style={{ lineHeight: '200px' }}>*No Items Added In Cart*</h5>
                        </Card.Body>
                    </Card>
                    :
                    products.map((product, index) => {
                        return (
                            <Card key={index}>
                                <Card.Body>
                                    <Row className='cart-row-body'>
                                        <Col xs={5} className='pl-5' style={{ textAlign: 'start' }}>
                                            <h5>{product.productName}</h5>
                                        </Col>
                                        <Col>
                                            <Form className='d-flex flex-row' id='product._id'>

                                                <Button onClick={() => handleMinus(product)}>-</Button>
                                                <Form.Control className="text-center" id={product._id} type="text" value={product.quantity} onChange={e => { }} />
                                                <Button onClick={() => handleAdd(product)}>+</Button>

                                            </Form>
                                        </Col>
                                        <Col>
                                            <h6>&#8369;{product.price}</h6>
                                        </Col>
                                        <Col>
                                            <h6>&#8369;{product.quantity * product.price}</h6>
                                        </Col>
                                        <Col>
                                            <Button variant='danger p-1' onClick={() => removeProduct(product)}>
                                                remove
                                            </Button>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        )
                    })
            }

            <Card>
                <Card.Body style={{ backgroundColor: 'rgb(100, 70, 260, 0.3)' }}>
                    <Row>
                        <Col><h4>Total Amount:</h4></Col>
                        <Col>
                            {
                                products
                                    ?
                                    products.length > 0
                                    &&
                                    <h3>&#8369;{products.reduce((price, item) => price + item.quantity * item.price, 0)}</h3>
                                    :
                                    <></>
                            }
                        </Col>
                        {
                            !products || products.length === 0 ?
                                <Col xs={7}><Button variant="success btn-block" onClick={() => checkOutOrder()} disabled>Check Out!</Button></Col>
                                :
                                <Col xs={7}><Button variant="success btn-block" onClick={() => checkOutOrder()}>Check Out!</Button></Col>
                        }
                    </Row>
                </Card.Body>
            </Card>
        </Container>
    )
}
