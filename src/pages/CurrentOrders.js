import { Container } from "react-bootstrap";
import { Card } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { useState, useEffect } from "react";


export default function CurrentOrders() {

    const [orders, setOrders] = useState();
    const userData = JSON.parse(sessionStorage.getItem('userData'))

    useEffect(() => {
        fetch('https://glacial-chamber-80958.herokuapp.com/users/getAllOrders', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${sessionStorage.getItem('accesstoken')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                let orders = []
                for (let i = 0; i < data.length; i++) {
                    const product = data[i].orders;
                    product.map((res) => {
                        orders.push(res);
                    })
                }
                const filtered = orders.filter((order) => {
                    if (userData) {
                        return order.name === `${userData[0].firstName} ${userData[0].middleInitial} ${userData[0].lastName}`
                    }
                })
                setOrders(filtered)
            })
    }, [])

    return (
        <Container id='main'>
            <h3 className='mt-5'>To Received Orders:</h3>
            <Card>
                <Card.Body>
                    <Row>
                        <Col><h6>Product Name</h6></Col>
                        <Col xs={2} className='text-center'><h6>xQuantity</h6></Col>
                        <Col xs={3} className='text-center'><h6>Price</h6></Col>
                        <Col xs={2} className='text-center'><h6>Subtotal</h6></Col>
                    </Row>
                </Card.Body>
                <Card>
                    {
                        orders ? orders.map((order, index) => {
                            return (
                                <Card.Body key={index} style={{ height: '100px' }}>
                                    <Row>
                                        <Col><h5 style={{ lineHeight: '50px' }}>{order.productName}</h5></Col>
                                        <Col xs={2} className='text-center'><h5 style={{ lineHeight: '50px' }}>x{order.quantity}</h5></Col>
                                        <Col xs={3} className='text-center'><h5 style={{ lineHeight: '50px' }}>{order.price}</h5></Col>
                                        <Col xs={2} className='text-center'><h5 style={{ lineHeight: '50px' }}>{order.quantity * order.price}</h5></Col>
                                    </Row>
                                </Card.Body>
                            )
                        }) :
                            <div></div>
                    }

                </Card>
                <Card.Footer>
                    <div className='d-flex flex-row justify-content-end'>
                        <h5>Grand Total:</h5>
                        <h5 className={'mx-5'}>{orders && orders.reduce((accum, next) => accum + next.quantity * next.price, 0)}</h5>
                    </div>
                </Card.Footer>
            </Card>
        </Container>
    )
}
