import { Container } from "react-bootstrap";
import Banner from "../components/Banner";
import { Card } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router";
import Swal from "sweetalert2";
import { userContext } from "../UserContext";

export default function Admin() {

    const [allproduct, setAllProduct] = useState();

    const history = useHistory();
    const { setEditProduct } = useContext(userContext);

    useEffect(() => {
        fetch('https://glacial-chamber-80958.herokuapp.com/products/allProducts', {
            method: "GET",
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('accesstoken')
            }
        })
            .then(res => res.json())
            .then(data => setAllProduct(data))
    }, [])

    const deleteProduct = (product) => {
        fetch('https://glacial-chamber-80958.herokuapp.com/products/deleteProduct', {
            method: "DELETE",
            headers: {
                Authorization: `Bearer ${sessionStorage.getItem('accesstoken')}`,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(product)
        })
            .then(res => res.json())
            .then(data => {
                Swal.fire({
                    title: 'Product deleted',
                    icon: 'success',
                    text: `The product ${data.name} is deleted permanently`
                }).then(() => {
                    window.location.href = '/admin'
                })
            })
    }

    let data = {
        title: "Seller's Page",
        tagline: 'You can sell here your high quality products to reach more buyers'
    }

    return (
        <Container id="main">
            <Banner data={data} />
            <div className='d-flex flex-row justify-content-between'>
                <h4>{allproduct && allproduct.length} Products</h4>
                <div>
                    <Button variant='success mr-3'>View Orders</Button>
                    <Button variant='primary' onClick={() => history.push('/createProduct')}>Create Product</Button>
                </div>
            </div>
            <Card className='bg-light'>
                <Card.Body>
                    <Row >
                        <Col className="text-start">Product Name</Col>
                        <Col xs={5} className="text-start">Description</Col>
                        <Col xs={1} className="text-center">Price</Col>
                        <Col xs={2} className="text-center">Status</Col>
                        <Col xs={1} className="text-center">Actions</Col>
                    </Row>
                </Card.Body>
            </Card>

            {
                allproduct && allproduct.map((product) => {
                    return (
                        <Card key={product._id} className=''>
                            <Card.Body>
                                <Row style={{ minHeight: '50px' }} className='d-flex align-items-center'>
                                    <Col>{product.name}</Col>
                                    <Col xs={5}>{product.description}</Col>
                                    <Col xs={1} className="text-center">{product.price}</Col>
                                    {
                                        product.isActive ?
                                            <Col xs={2} className="text-center">Active</Col>
                                            :
                                            <Col xs={2} className="text-center">Inactive</Col>
                                    }
                                    <Col xs={1} className="d-flex flex-column align-items-center">
                                        <Button variant={'secondary mb-1'} style={{ lineHeight: '10px', width: '80px' }} onClick={() => {
                                            setEditProduct(product);
                                            history.push('/editProduct');
                                        }}>Edit</Button>
                                        <Button variant={'danger'} style={{ lineHeight: '10px', width: '80px', padding: '5px 1px' }} onClick={() => deleteProduct(product)} >Delete</Button>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    )
                })
            }

        </Container>
    )
}
