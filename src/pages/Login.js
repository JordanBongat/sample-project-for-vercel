import { Container } from "react-bootstrap";
import Banner from "../components/Banner";
import LoginForm from "../components/LoginForm";
import { useHistory } from "react-router";
import Swal from "sweetalert2";


export default function Login() {

    const history = useHistory()

    const login = (input) => {
        fetch('https://glacial-chamber-80958.herokuapp.com/users/login', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(input[0])
        }).then(res => res.json()).then(data => {
            sessionStorage.setItem('accesstoken', data.accessToken)

            if (data.accessToken === undefined) {
                Swal.fire({
                    icon: 'error',
                    title: 'Owshii..',
                    text: data.value,
                    footer: '<a href="">Forgot Password</a>'
                })

                input[1].setEmail('');
                input[1].setPassword('');
            } else {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 1600,
                    timerProgressBar: true
                })

                Toast.fire({
                    icon: 'success',
                    title: 'Signed in successfully'
                }).then(() => {
                    history.push('/')
                })

                retrieveUserDetails(data.accessToken)
            }
        })
    }

    const retrieveUserDetails = (token) => {
        fetch('https://glacial-chamber-80958.herokuapp.com/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(res => res.json())
            .then(data => sessionStorage.setItem('userData', JSON.stringify(data)))
    }

    const loginBanner = {
        title: 'Login Now',
        tagline: 'Login to start placing your orders'
    }

    return (
        <Container id='main'>
            <Banner data={loginBanner} margtop={'3vh'} />
            <LoginForm userInput={login} />
        </Container>
    )
}
