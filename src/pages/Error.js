import { Container } from "react-bootstrap";

export default function Error() {
    return (
        <Container id='main' className="text-center text-danger" style={{ marginTop: '25vh' }}>
            <h1>
                404 Error-Page Not Found
            </h1>
            <p>
                You are accessing an Invalid Website
            </p>
        </Container>
    )
}
