import { Container } from "react-bootstrap";
import Banner from "../components/Banner";
import RegisterForm from "../components/RegisterForm";

export default function Register({ register }) {

    const registerBanner = {
        title: 'Register Now!',
        tagline: 'Fill-out the form below to make an account'
    }

    return (
        <Container id='main'>
            <Banner data={registerBanner} />
            <RegisterForm register={register} />
        </Container>
    )
}
