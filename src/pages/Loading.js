import { Container } from "react-bootstrap";

export default function Loading() {
    return (
        <Container id='main'>
            <h1 className="text-center text-primary" style={{ marginTop: '25vh', fontSize: '70px' }}>
                Loading...
            </h1>
            <h2 className="text-center">(Please wait for a moment)</h2>
        </Container>
    )
}
