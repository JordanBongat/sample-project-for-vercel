import { Container } from "react-bootstrap";
import Banner from "../components/Banner";
import { Card } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { useHistory } from "react-router";
import { Button } from "react-bootstrap";

export default function OrderHistory() {

    const history = useHistory();

    let orderBanner = {
        title: 'Order History',
        tagline: 'Here you can view or track your current orders and received orders.'
    }

    const currentOrders = () => {
        history.push('/currentOrders')
    }

    return (
        <Container id='main'>
            <Banner data={orderBanner} />
            <Row>
                <Col onClick={() => currentOrders()}>
                    <Button variant='light'>
                        <Card style={{ height: '200px', width: '300px' }}>
                            <Card.Body className='d-flex flex-column justify-content-center text-center align-items-center' >
                                <div>
                                    <img src="./delivericon.png" alt="deliver" />
                                </div>
                                <div>Current Order/s</div>
                            </Card.Body>
                        </Card>
                    </Button>
                </Col>
                <Col className='d-flex justify-content-center'>
                    <img src="./arrowicon.png" alt="arrow" />
                </Col>
                <Col>
                    <Button variant='light'>
                        <Card className='text-center' style={{ height: '200px', width: '300px' }}>
                            <Card.Body className='d-flex flex-column justify-content-center text-center align-items-center'>
                                <div>
                                    <img src="./receivedicon.png" alt="received" />
                                </div>
                                <div>Received Order/s</div>
                            </Card.Body>
                        </Card>
                    </Button>
                </Col>
            </Row>
        </Container>
    )
}
