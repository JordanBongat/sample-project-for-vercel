import { Container } from "react-bootstrap";
import Banner from "../components/Banner";
import Products from "../components/Products";


export default function Home({ products, select }) {

    const homeBanner = {
        title: 'Welcome Everyone!',
        tagline: 'This is the best ecommerce app where you can find high quality products'
    }

    return (
        <Container id="main">
            <Banner data={homeBanner} />
            <div className='d-flex flex-row flex-wrap justify-content-start'>
                {
                    products.map((product) => {
                        return <Products data={product} id={product._id} key={product._id} select={select} />
                    })
                }
            </div>
        </Container>
    )
}
