import { Container } from "react-bootstrap";
import { Form } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { userContext } from "../UserContext";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { Redirect } from "react-router";

export default function EditProduct() {

    const { editProduct } = useContext(userContext);

    const [productName, setProductName] = useState();
    const [description, setDescription] = useState();
    const [price, setPrice] = useState();

    const updateProduct = (e) => {
        e.preventDefault();
        fetch(`https://glacial-chamber-80958.herokuapp.com/products/updateProduct/${editProduct._id}`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${sessionStorage.getItem('accesstoken')}`,
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                name: productName,
                description: description,
                price: price
            })
        })

        Swal.fire({
            title: 'Ok Goods!',
            icon: 'success',
            text: 'The product update is successfull'
        }).then(() => {
            window.location.href = '/admin'
        })
    }

    const deactivate = (id) => {
        fetch(`https://glacial-chamber-80958.herokuapp.com/products/archiveProduct/${id}`, {
            method: "PUT",
            headers: {
                Authorization: `Bearer ${sessionStorage.getItem('accesstoken')}`,
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                isActive: false
            })
        })

        Swal.fire({
            title: 'Done!',
            icon: 'success',
            text: 'The product is deactivated'
        }).then(() => {
            window.location.href = '/admin'
        })
    }

    const activate = (id) => {
        fetch(`https://glacial-chamber-80958.herokuapp.com/products/archiveProduct/${id}`, {
            method: "PUT",
            headers: {
                Authorization: `Bearer ${sessionStorage.getItem('accesstoken')}`,
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                isActive: true
            })
        })

        Swal.fire({
            title: 'Alright!',
            icon: 'success',
            text: 'The product is activated'
        }).then(() => {
            window.location.href = '/admin'
        })
    }


    return (
        editProduct ?
            < Container id='main' >
                <h1 className='my-5'>Edit Product</h1>
                <Form onSubmit={e => updateProduct(e)}>
                    <Form.Group>
                        <Form.Label>
                            Product Name:
                        </Form.Label>
                        <Form.Control type='text' value={productName === undefined ? editProduct.name : productName} onChange={e => setProductName(e.target.value)} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>
                            Description:
                        </Form.Label>
                        <Form.Control type='text' value={description === undefined ? editProduct.description : description} onChange={e => setDescription(e.target.value)} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>
                            Price:
                        </Form.Label>
                        <Form.Control type='number' value={price === undefined ? editProduct.price : price} onChange={e => setPrice(e.target.value)} />
                    </Form.Group>
                    <div className='d-flex justify-content-end mt-4'>
                        {
                            editProduct.isActive
                                ?
                                <Button variant='warning mr-3' onClick={() => deactivate(editProduct._id)}>Deactivate</Button>
                                :
                                <Button variant='warning mr-3' onClick={() => activate(editProduct._id)}>Activate</Button>
                        }
                        <Button type="submit" variant='primary'>Update Product</Button>
                    </div>
                </Form>
            </Container >
            :
            <Redirect to='/admin' />
    )
}
