import React from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import AppFooter from './components/AppFooter';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import SingleProduct from './pages/SingleProduct';
import Profile from './pages/Profile';
import Cart from './pages/Cart';
import OrderHistory from './pages/OrderHistory';
import CurrentOrders from './pages/CurrentOrders';
import Admin from './pages/Admin';
import CreateProduct from './pages/CreateProduct';
import EditProduct from './pages/EditProduct';
import Loading from './pages/Loading';
import Error from './pages/Error';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { userContext } from './UserContext';

function App() {

  const [products, setProducts] = useState([]);
  const [product, setProduct] = useState([]);

  const [editProduct, setEditProduct] = useState('')

  useEffect(() => {
    fetch('https://glacial-chamber-80958.herokuapp.com/products/activeProducts').then(result => result.json()).then(results => { setProducts(results) }).catch(() => window.location.href = '/error')
  }, [])

  const fetchProduct = async (id) => {
    await fetch(`https://glacial-chamber-80958.herokuapp.com/products/singleProduct/${id}`).then(result => result.json()).then(single => { setProduct(single) }).catch(() => window.location.href = '/error')
  }

  const register = async (data) => {
    await fetch('https://glacial-chamber-80958.herokuapp.com/users/register', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(data)
    })
  }

  return (
    <BrowserRouter>
      <userContext.Provider value={{ editProduct, setEditProduct }}>
        <AppNavbar />

        <Switch>
          <Route exact path='/' render={() => <Home products={products} select={fetchProduct} />} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/register' render={() => <Register register={register} />} />
          <Route exact path={`/product/${product._id}`} render={() => <SingleProduct data={product} />} />
          <Route path='/product/' component={Loading} />
          <Route exact path={'/profile'} component={Profile} />
          <Route exact path={'/cart'} component={Cart} />
          <Route exact path={'/currentOrders'} component={CurrentOrders} />
          <Route exact path={'/order'} component={OrderHistory} />
          <Route exact path={'/admin'} component={Admin} />
          <Route exact path={'/createProduct'} component={CreateProduct} />
          <Route exact path={'/editProduct'} component={EditProduct} />
          <Route path='/' component={Error} />
        </Switch>

        <AppFooter />
      </userContext.Provider>
    </BrowserRouter>
  );
}

export default App;
